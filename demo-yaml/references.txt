References:
---------------------------------------------------------------------------------
Docs - Operator pattern:
https://kubernetes.io/docs/concepts/extend-kubernetes/operator

Docs - Kubebuilder book:
https://book.kubebuilder.io/

Docs - Operator-SDK:
https://kubernetes.io/docs/concepts/extend-kubernetes/operator

Docs - Operator levels:
https://sdk.operatorframework.io/docs/overview/operator-capabilities


---------------------------------------------------------------------------------
Example - Trigger reconcilation loop with a timer:
https://gist.github.com/yashvardhan-kukreja/c49ad6ad64de2aafe054478bbfbd9934#file-mycontroller-go

Example - Write an Operator in Rust
https://news.ycombinator.com/item?id=35608624

---------------------------------------------------------------------------------
Article - Demystifying the For vs Owns vs Watches controller-builders in controller-runtime:
https://yash-kukreja-98.medium.com/develop-on-kubernetes-series-demystifying-the-for-vs-owns-vs-watches-controller-builders-in-c11ab32a046e

Article - Build a Kubernetes Operator in 10 Minutes:
https://betterprogramming.pub/build-a-kubernetes-operator-in-10-minutes-11eec1492d30

Article - Inside of Kubernetes Controller:
https://speakerdeck.com/govargo/inside-of-kubernetes-controller

Article - From Zero to Kubernetes Operator:
https://medium.com/@victorpaulo/from-zero-to-kubernetes-operator-dd06436b9d89

Article - Deploying Kubebuilder Operators to Kubernetes
https://medium.com/@marom.itamar/deploying-kubebuilder-operators-to-kubernetes-547ee7fe59a9
