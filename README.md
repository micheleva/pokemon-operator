# pokemon-operator
A very simple operator to be used for demoing.

## Description
(The operator is not fully functional: just the very basic blocs are implemented, again it's a exteremly-bare-bone demo)
Pokémon © 2002-2023 Pokémon. © 1995-2023 Nintendo/Creatures Inc./GAME FREAK inc. TM, ® and Pokémon character names are trademarks of Nintendo.


No copyright or trademark infringement is intended in using Pokémon content in this repository.


## Getting Started
You’ll need a Kubernetes cluster to run against. You can use [KIND](https://sigs.k8s.io/kind) to get a local cluster for testing, or run against a remote cluster.
**Note:** Your controller will automatically use the current context in your kubeconfig file (i.e. whatever cluster `kubectl cluster-info` shows).


```sh
# Create a kind cluster
kind create cluster -n demo-cluster --config=./demo-yaml/step-0-create-kind-cluster.yaml

# Install nginx-controller and cert-manager
kubectl apply -f demo-yaml/step-1-nginx-ingress-controller-deploy.yaml
kubectl apply -f demo-yaml/step-2-install-cert-manager.yaml
```


### Running on the cluster
1. Install Instances of Custom Resources:

```sh
kubectl apply -f config/samples/
```

2. Build and push your image to the location specified by `IMG`:

```sh
make docker-build docker-push IMG=<some-registry>/pokemon-operator:tag
```

3. Deploy the controller to the cluster with the image specified by `IMG`:

```sh
make deploy IMG=<some-registry>/pokemon-operator:tag
```

### Uninstall CRDs
To delete the CRDs from the cluster:

```sh
make uninstall
```

### Undeploy controller
UnDeploy the controller from the cluster:

```sh
make undeploy
```

## Contributing
// TODO(user): Add detailed information on how you would like others to contribute to this project

### How it works
This project aims to follow the Kubernetes [Operator pattern](https://kubernetes.io/docs/concepts/extend-kubernetes/operator/).

It uses [Controllers](https://kubernetes.io/docs/concepts/architecture/controller/),
which provide a reconcile function responsible for synchronizing resources until the desired state is reached on the cluster.

### Test It Out
1. Install the CRDs into the cluster:

```sh
make install
```

2. Run your controller (this will run in the foreground, so switch to a new terminal if you want to leave it running):

```sh
make run
```

**NOTE:** You can also run this in one step by running: `make install run`

### Modifying the API definitions
If you are editing the API definitions, generate the manifests such as CRs or CRDs using:

```sh
make manifests
```

**NOTE:** Run `make --help` for more information on all potential `make` targets

More information can be found via the [Kubebuilder Documentation](https://book.kubebuilder.io/introduction.html)


## Commands used to generate the scaffolding

```sh
kubebuilder init --domain michele.example.com --repo michele.example.com/pokemon
kubebuilder create api --group pokemon --version v1 --kind Pokedex --shortName poke
kubebuilder create api --group pokemon --version v1 --kind Pokedex --plural=pokedexes --force

# Before creating the hook, this is how to run the controller from "outside" kind
make run # or `go run .`

# Create the hook
kubebuilder create webhook --group pokemon --version v1 --kind Pokedex --plural pokedexes --programmatic-validation

# Add markers on the hook
vim api/v1/pokedex_webhook.go

# Comment out lines related to webhook and cert-manager, to enable said patches
vim config/default/kustomization.yaml
make manifests
make install

# Install cert-manager https://medium.com/@marom.itamar/deploying-kubebuilder-operators-to-kubernetes-547ee7fe59a9
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.9.1/cert-manager.yaml

# After creating the webhook
make run ENABLE_WEBHOOKS=true # Note this will fail atm, as we need to copy the cert from the inside kind's containers to the host (e.g. laptop)
make webhook

# ------------------------------
# To run the operator inside kind, as a pod in the "pokemon-operator-system" namespace, execute the following comands:

# Do not use "latest" tag unless imagePullPolicy: ifNotPresent or Never is specified (this is due to how k8s imagePullPolicy works)
# See https://github.com/kubernetes-sigs/kind/issues/2474
# See https://kubernetes.io/docs/concepts/containers/images/#updating-images
make
make docker-build IMG=pokemon/controller:0.0.X
kind load IMG=pokemon/controller:0.0.X

# Confirm it was loaded
docker exec -it kind-control-plane crictl images
make deploy IMG=pokemon/controller:0.0.X

```


## Memo to self


```sh
# Create a kind cluster
kind create cluster -n demo-cluster --config=./demo-yaml/step-0-create-kind-cluster.yaml

# Install nginx-controller and cert-manager
kubectl apply -f demo-yaml/step-1-nginx-ingress-controller-deploy.yaml
kubectl apply -f demo-yaml/step-2-install-cert-manager.yaml

# Create a Pokemon CRD
kubectl apply -f demo-yaml/<pokemon-name>/pokemon.yaml

# Create a Pokedex CRD
kubectl apply -f demo-yaml/pokedex.yaml

# Wait for the pod to be spawned
kubectl exec <pokemon-name>-busybox -- wget -qO- localhost

# Access to it from the dev machine relying on ingress
curl localhost/<name-of-the-matching-ingress-path-for-the-service-name>

# In case of problem, troubleshoot with
kubectl get pods -o wide
kubectl get svc -o wide
kubectl get endpoints <service-name>
docker exec -it kind-control-plane curl <pod-ip>
docker exec -it kind-control-plane curl <svc-ip>
```


## License

Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

