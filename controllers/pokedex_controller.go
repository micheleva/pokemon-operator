/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"

	pokemonv1 "michele.example.com/pokemon/api/v1"
)

// PokedexReconciler reconciles a Pokedex object
type PokedexReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=pokemon.michele.example.com,resources=pokedexes,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=pokemon.michele.example.com,resources=pokedexes/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=pokemon.michele.example.com,resources=pokedexes/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Pokedex object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *PokedexReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := log.FromContext(ctx)
	log.V(1).Info("reconciling pokedex custom resource")
	// Load the pokedex that triggered this reconciliation loop
	var pokedex pokemonv1.Pokedex
	if err := r.Get(ctx, req.NamespacedName, &pokedex); err != nil {
		// Ignore the error, the pokemon has been deleted while the loop was running
		// DO NOT return here, as we have to update the pokedex status!
		log.Info("unable to get the request item tha triggered the pokedex reconciler, as it was deleted: ignoring...")
	}
	log.V(1).Info(fmt.Sprintf("%v", pokedex))

	// List all pokemon in this namespace
	var allPokemon pokemonv1.PokemonList
	if err := r.List(ctx, &allPokemon, client.InNamespace(req.Namespace)); err != nil {
		log.Error(err, "unable to list all pokemon")
		return ctrl.Result{}, err
	}

	// TODO: remove the bolean and use instead:
	// map[string]struct{} => this way, "values" will use no memory
	names := make(map[string]struct{})
	for _, pokemon := range allPokemon.Items {
		names[pokemon.Spec.Name] = struct{}{}
	}

	uniquePokemons := []string{}
	for name := range names {
		uniquePokemons = append(uniquePokemons, name)
	}

	log.Info(fmt.Sprintf("All Pokemon count: %v, Unique Pokemon: %v ", len(names), len(uniquePokemons)))

	pokedex.Status.Completion = fmt.Sprintf("%d/151", len(uniquePokemons))
	if err := r.Status().Update(ctx, &pokedex); err != nil {
		log.Error(err, "unable to update Pokedex status")
		log.V(1).Info(fmt.Sprintf("%v", pokedex.Spec))
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

// As we're watching Pokemon CRD in the Watchs() func, we need to interpolate Pokemon to Pokedex,
// otherwiser the Reconciler's req argument will contain the Pokemon info
func (r *PokedexReconciler) MapPokemonRequestToPokedexReq(obj client.Object) []reconcile.Request {
	ctx := context.Background()
	log := log.FromContext(ctx)

	// List all the Pokedex resources (there should be only 1)
	req := []reconcile.Request{}
	var list pokemonv1.PokedexList
	if err := r.Client.List(context.TODO(), &list); err != nil {
		log.Error(err, "unable to list all pokedexes")
	} else {
		// Only keep Pokemon resources related to the Pod that triggered the reconciliation request
		log.Info(fmt.Sprintf("We have exactly %v pokedex(es)", len(list.Items)))
		for _, item := range list.Items {
			req = append(req, reconcile.Request{
				NamespacedName: types.NamespacedName{Name: item.Name, Namespace: item.Namespace},
			})
			log.Info("A Pokemon CRD change triggered the Pokedex mapFunction", "name", obj.GetName())
		}
	}
	return req
}

// SetupWithManager sets up the controller with the Manager.
func (r *PokedexReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&pokemonv1.Pokedex{}).
		Watches(&source.Kind{Type: &pokemonv1.Pokemon{}}, handler.EnqueueRequestsFromMapFunc(r.MapPokemonRequestToPokedexReq)).
		Complete(r)
}
