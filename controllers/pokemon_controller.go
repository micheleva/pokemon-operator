/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	pokemonv1 "michele.example.com/pokemon/api/v1"
)

// PokemonReconciler reconciles a Pokemon object
type PokemonReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=pokemon.michele.example.com,resources=pokemons,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=pokemon.michele.example.com,resources=pokemons/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=pokemon.michele.example.com,resources=pokemons/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.

// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *PokemonReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := log.FromContext(ctx)
	log.V(1).Info("reconciling pokemon custom resource")

	// Load the pokemon that triggered this reconciliation loop
	var pokemon pokemonv1.Pokemon
	if err := r.Get(ctx, req.NamespacedName, &pokemon); err != nil {
		// Ignore the error, the pokemon has been deleted while the loop was running
		log.Info("unable to update Pokemon status, as it was deleted: ignoring...")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	// If the pokemon was just created, it is not marked as catched yet: catch it!
	if pokemon.Status.Catched != true {
		pokemon.Status.Catched = true
	}

	if err := r.Status().Update(ctx, &pokemon); err != nil {
		log.Error(err, "unable to update Pokemon status")
		return ctrl.Result{}, err
	}

	// Let's create a pod for this pokemon!

	// NICE TO HAVE: add a container field in the spec, and accept nginx or busybox
	// NICE TO HAVE: add a image url, and create a configmap with the image (?) or just add the link to the page

	// Generate the HTML content for the busybox container
	htmlContent := fmt.Sprintf("<html><body>%s</body></html>", pokemon.Spec.Name)

	// Define the busybox container
	busyboxContainer := corev1.Container{
		Name:  "busybox",
		Image: "busybox",
		Command: []string{
			"/bin/sh",
			"-c",
			fmt.Sprintf("echo -e 'HTTP/1.1 200 OK\r\n\r\n%s' | nc -l -p 80", htmlContent),
		},
		Ports: []corev1.ContainerPort{
			{
				ContainerPort: 80,
				Protocol:      corev1.ProtocolTCP,
			},
		},
	}

	// Define the busybox pod
	busyboxPod := corev1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-busybox", pokemon.Spec.Name),
			Namespace: pokemon.Namespace,
			Labels: map[string]string{
				"app":           pokemon.Spec.Name,
				"pokemon":       pokemon.Spec.Name,
				"pokemon-route": pokemon.Spec.Name,
			},
		},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				busyboxContainer,
			},
		},
	}

	// Set this Pokemon instance (the one that triggered the reconciler) as the owner of the busybox pod
	if err := ctrl.SetControllerReference(&pokemon, &busyboxPod, r.Scheme); err != nil {
		return ctrl.Result{}, err
	}

	// Check if the busybox pod already exists
	podKey := types.NamespacedName{Name: busyboxPod.Name, Namespace: busyboxPod.Namespace}
	existingPod := &corev1.Pod{}
	// TODO: also check if we have too many pods, and delete the un-necessary ones
	// TODO: confirm the pod has not been altered (i.e. labels, etc.)
	err := r.Get(ctx, podKey, existingPod)
	if err != nil {
		if errors.IsNotFound(err) {
			// Busybox pod doesn't exist, create it
			err = r.Create(ctx, &busyboxPod)
			if err != nil {
				log.Error(err, "Error creating busybox pod")
				return ctrl.Result{}, err
			} else {
				log.Info("Created busybox pod")
			}
		} else {
			// Error fetching busybox pod, requeue
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

var (
	jobOwnerKey = ".metadata.controller"
	apiGVStr    = pokemonv1.GroupVersion.String()
)

// SetupWithManager sets up the controller with the Manager.
func (r *PokemonReconciler) SetupWithManager(mgr ctrl.Manager) error {

	if err := mgr.GetFieldIndexer().IndexField(context.Background(), &pokemonv1.Pokemon{}, jobOwnerKey, func(rawObj client.Object) []string {
		pok := rawObj.(*pokemonv1.Pokemon)
		owner := metav1.GetControllerOf(pok)
		if owner == nil {
			return nil
		}
		if owner.APIVersion != apiGVStr || owner.Kind != "Pokemon" {
			return nil
		}
		return []string{owner.Name}
	}); err != nil {
		return err
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&pokemonv1.Pokemon{}).
		Owns(&corev1.Pod{}). // trigger the Pokemon(r).Reconcile whenever a busybox-pod is created/updated/deleted
		Complete(r)
}
