/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	"context"
	"errors"
	"fmt"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

var (
	pokedexlog = logf.Log.WithName("pokedex-resource")
	// See https://github.com/kubernetes-sigs/kubebuilder/issues/1216
	myClient client.Client
)

func (r *Pokedex) SetupWebhookWithManager(mgr ctrl.Manager) error {
	myClient = mgr.GetClient()

	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

// TODO(user): change verbs to "verbs=create;update;delete" if you want to enable deletion validation.
//+kubebuilder:webhook:path=/validate-pokemon-michele-example-com-v1-pokedex,mutating=false,failurePolicy=fail,sideEffects=None,groups=pokemon.michele.example.com,resources=pokedexes,verbs=create;update,versions=v1,name=vpokedex.kb.io,admissionReviewVersions=v1
//+kubebuilder:rbac:groups=pokemon.michele.example.com,resources=pokemon,verbs=get;list;watch
//+kubebuilder:rbac:groups="core",resources=pods,verbs=create;update;get;list;watch

var _ webhook.Validator = &Pokedex{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *Pokedex) ValidateCreate() error {
	pokedexlog.Info("validate create", "name", r.Name)

	// Check if this namespace has a pokedex already
	var pokedexList PokedexList
	if err := myClient.List(context.Background(), &pokedexList, &client.ListOptions{Namespace: r.Namespace}); err != nil {
		return err
	}
	if len(pokedexList.Items) > 0 {
		return errors.New("a Pokedex already exists in this namespace")
	}

	pokedexlog.Info(fmt.Sprintf("%v", pokedexList))
	return nil
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *Pokedex) ValidateUpdate(old runtime.Object) error {
	pokedexlog.Info("validate update", "name", r.Name)

	// TODO(user): fill in your validation logic upon object update.
	return nil
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *Pokedex) ValidateDelete() error {
	pokedexlog.Info("validate delete", "name", r.Name)

	// TODO(user): fill in your validation logic upon object deletion.
	return nil
}
