/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// PokemonSpec defines the desired state of Pokemon
type PokemonSpec struct {
	// Name of the pokemon: charmender, squirtle, bulbasaur, etc.
	Name string `json:"name"`
	// Type of the pokemon: "fire", "water", "thunder", etc.
	Type string `json:"type"`
	// +kubebuilder:validation:Minimum=1
	// +kubebuilder:validation:Maximum=99
	// Level of the pokemon: 1-99
	Level int `json:"level"`
}

// PokemonStatus defines the observed state of Pokemon
type PokemonStatus struct {
	Catched bool `json:"catched"`

	// TODO add Friends string[] and store here the other same type pokemon
	// assuming we have charmender, Pikachu and Zapods pokemond CRD created in this namespace
	// Then, thunder type pokemon Pikachu will see in the friends field Zapdos (thunder), but not charmender (fire)
	// On the other hand charmender will see have an empty friendsfield

	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

// See https://book.kubebuilder.io/reference/markers/crd.html for additional markers
//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:printcolumn:name="Pokemon",type="string",JSONPath=".spec.name",description="Name of the pokemon"
//+kubebuilder:printcolumn:name="Type",type="string",JSONPath=".spec.type",description="Type of the pokemon"
//+kubebuilder:printcolumn:name="Level",type="integer",JSONPath=".spec.level",description="Level of the pokemon"
//+kubebuilder:printcolumn:name="Catched",type="boolean",JSONPath=".status.catched",description="Catched flag"
//+kubebuilder:printcolumn:name="Age",type="date",JSONPath=".metadata.creationTimestamp",description="Creation time"
//+kubebuilder:printcolumn:name="UID",type="string",priority=1,JSONPath=".metadata.uid",description="Unique identifier"
//+kubebuilder:resource:shortName=poke,categories=all
// NOTE: shortName and categories must be in the same line, contrary to printcolumn. Otherwise kubebuilder will just use the last one

// Pokemon is the Schema for the pokemons API
type Pokemon struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   PokemonSpec   `json:"spec,omitempty"`
	Status PokemonStatus `json:"status,omitempty"`
}

func (p *Pokemon) GetObjectMeta() metav1.Object {
	return &p.ObjectMeta
}

//+kubebuilder:object:root=true

// PokemonList contains a list of Pokemon
type PokemonList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Pokemon `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Pokemon{}, &PokemonList{})
}
