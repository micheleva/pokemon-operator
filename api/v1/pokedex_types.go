/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// PokedexSpec defines the desired state of Pokedex
type PokedexSpec struct {
	// Trainer is the name of the Pokedex owner
	Trainer string `json:"trainer_name,omitempty"`
}

// PokedexStatus defines the observed state of Pokedex
type PokedexStatus struct {
	Completion string `json:"completion"`
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:printcolumn:name="Completion",type="string",JSONPath=".status.completion",description="Catched pokemon"
//+kubebuilder:printcolumn:name="Age",type="date",JSONPath=".metadata.creationTimestamp",description="Creation time"
//+kubebuilder:printcolumn:name="Trainer",type="string",priority=1,JSONPath=".spec.trainer",description="Name of the pokedex owner"
//+kubebuilder:printcolumn:name="UID",type="string",priority=1,JSONPath=".metadata.uid",description="Unique identifier"
//+kubebuilder:resource:categories=all

// Pokedex is the Schema for the pokedexes API
type Pokedex struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	// TODO make pokedex CRD unique per namespace
	Spec   PokedexSpec   `json:"spec,omitempty"`
	Status PokedexStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// PokedexList contains a list of Pokedex
type PokedexList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Pokedex `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Pokedex{}, &PokedexList{})
}
